//
//  AppChatApp.swift
//  AppChat
//
//  Created by Tran Thuan on 23/02/2022.
//

import SwiftUI
import Firebase

@main
struct AppChatApp: App {
    init() {
        FirebaseApp.configure()
      }
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
