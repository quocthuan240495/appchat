//
//  ChatMessage.swift
//  AppChat
//
//  Created by Tran Thuan on 03/03/2022.
//

import Foundation
import FirebaseFirestoreSwift

struct ChatMessage: Codable, Identifiable {
    @DocumentID var id: String?
    let fromId, toId, text: String, imgMessage: String
    let timestamp: Date
}
