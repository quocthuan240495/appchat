//
//  ChatUser.swift
//  AppChat
//
//  Created by Tran Thuan on 03/03/2022.
//

import FirebaseFirestoreSwift

struct ChatUser: Codable, Identifiable {
    @DocumentID var id: String?
    let uid, email, name, profileImageUrl: String
}

