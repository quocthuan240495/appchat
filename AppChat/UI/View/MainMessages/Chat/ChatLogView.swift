//
//  ChatLogView.swift
//  AppChat
//
//  Created by Tran Thuan on 03/03/2022.
//

import SwiftUI
import Firebase
import SDWebImageSwiftUI

struct ChatLogView: View {
    
    @ObservedObject var vm: ChatLogViewModel
    @State var imageMessage: UIImage?
        
    var body: some View {
        ZStack {
            messagesView
            Text(vm.errorMessage)
        }
        .navigationTitle(vm.chatUser?.name ?? "")
        .navigationBarTitleDisplayMode(.inline)
        .onDisappear {
            vm.firestoreListener?.remove()
        }
        .fullScreenCover(isPresented: $vm.isShowImagePickerMessage, onDismiss: {
            if imageMessage != nil
            {
                let _ = print("image123: \(imageMessage)")
                vm.handleSendImage(imageMessage: imageMessage)
            }
        }) {
            ImagePickerMessage(imageMessage: $imageMessage)
        }
    }
    
    static let emptyScrollToString = "Empty"
    
    private var messagesView: some View {
        VStack {
            if #available(iOS 15.0, *) {
                ScrollView {
                    ScrollViewReader { scrollViewProxy in
                        VStack {
                            ForEach(vm.chatMessages) { message in
                                if message.fromId == FirebaseManager.shared.auth.currentUser?.uid {
                                    HStack {
                                        Spacer()
                                        HStack {
                                            
                                            let text = message.text
                                            let _ = print("message.imgMessage: \(message.imgMessage)")
                                            if text == "" {
                                                
                                                WebImage(url: URL(string: message.imgMessage ))
                                                    .resizable()
                                                    .scaledToFit()
                                                    .cornerRadius(15)
                                                    .frame(maxWidth: UIScreen.main.bounds.width - 150)
                                                
                                            } else {
                                                
                                                Text(text)
                                                    .padding()
                                                    .foregroundColor(.white)
                                                    .background(Color.blue)
                                                    .cornerRadius(8)
                                            }
                                        }
                                        .padding()
                                        .background(Color.clear)
                                    }
                                } else {
                                    HStack {
                                        HStack {
                                            
                                            let text = message.text
                                            let _ = print("message.imgMessage: \(message.imgMessage)")
                                            if text == "" {
                                                
                                                WebImage(url: URL(string: message.imgMessage ))
                                                    .resizable()
                                                    .scaledToFit()
                                                    .cornerRadius(15)
                                                    .frame(maxWidth: UIScreen.main.bounds.width - 150)
                                                
                                            } else {
                                                
                                                Text(text)
                                                    .padding()
                                                    .foregroundColor(.white)
                                                    .background(Color.gray)
                                                    .cornerRadius(8)
                                            }
                                        }
                                        .padding()
                                        .background(Color.clear)
                                        Spacer()
                                    }
                                }
                            }
                            
                            HStack{ Spacer() }
                            .id(Self.emptyScrollToString)
                        }
                        .onReceive(vm.$count) { _ in
                            withAnimation(.easeOut(duration: 0.5)) {
                                scrollViewProxy.scrollTo(Self.emptyScrollToString, anchor: .bottom)
                            }
                        }
                    }
                }
                .background(Color(.init(white: 0.95, alpha: 1)))
                .safeAreaInset(edge: .bottom) {
                    chatBottomBar
                        .background(Color(.systemBackground).ignoresSafeArea())
                }
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    private var chatBottomBar: some View {
        HStack(spacing: 16) {
            Button {
                
                vm.isShowImagePickerMessage = true
                
            } label: {
                
                Image(systemName: "photo.on.rectangle.angled")
                    .font(.system(size: 20, weight: .bold))
                    .foregroundColor(.purple)
                
            }
            ZStack {
                DescriptionPlaceholder()
                TextEditor(text: $vm.chatText)
                    .opacity(vm.chatText.isEmpty ? 0.5 : 1)
            }
            .frame(height: 40)
            
            Button {
                vm.handleSendMessage(imageProfileUrl: "")
            } label: {
                Text("Send")
                    .foregroundColor(.white)
            }
            .padding(.horizontal)
            .padding(.vertical, 8)
            .background(Color.blue)
            .cornerRadius(4)
        }
        .padding(.horizontal)
        .padding(.vertical, 8)
    }
}

private struct DescriptionPlaceholder: View {
    var body: some View {
        HStack {
            Text("Description")
                .foregroundColor(Color(.gray))
                .font(.system(size: 17))
                .padding(.leading, 5)
                .padding(.top, -4)
            Spacer()
        }
    }
}

struct ChatLogView_Previews: PreviewProvider {
    static var previews: some View {
        MainMessagesView()
    }
}
