////
////  ChatListView.swift
////  AppChat
////
////  Created by Tran Thuan on 25/02/2022.
////
//
//
//import SwiftUI
//
//struct ChatListView: View {
//    
//    @State var searchText: String = ""
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
//    @ObservedObject private var kGuardian = KeyboardGuardian(textFieldCount: 1)
//    
//    let users = ["Shezad", "Mathew", "Afna", "Jerin", "Catherine"]
//    let messages = [
//        ["user":"Catherine", "message":"Hi there, How's your work ? did you completed that cross platform app ? ", "time": "10:30 AM"],
//        ["user":"Shezad", "message": "Are you available tomorrow at 3:30 pm ? i'd like to discuss about our new project", "time": "12:45 AM"],
//        ["user":"Afna", "message": "Hey, is there any update for morning stand up meeting tomorrow ?", "time": "12:15 PM"],
//        ["user":"Mathew", "message": "Wow, awesome! Thank you so much for your effort", "time": "4:30 AM"],
//        ["user":"Jerin", "message": "Yeah, Let's meet tomrrow evening 5:30pm at coffe shop", "time": "8:17 AM"]]
//    
//    
//    var body: some View {
//        NavigationView {
//            ZStack{
//                Color("color_bg").edgesIgnoringSafeArea(.all)
//                VStack{
//                    HStack{
//                        Image("back")
//                            .frame(width: 10, height: 10, alignment: SwiftUI.Alignment.center)
//                            .foregroundColor(Color("PrimaryColor"))
//                            .onTapGesture {
//                                self.presentationMode.wrappedValue.dismiss()
//                            }
//                        Text("Chats")
//                            .fontWeight(.semibold)
//                            .font(.largeTitle)
//                        Spacer()
//                        Image(systemName: "square.and.pencil")
//                            .foregroundColor(Color("PrimaryColor"))
//                            .font(.title2)
//                    }
//                    
//                    ScrollView(showsIndicators: false){
//                        VStack(alignment: .leading, spacing: 5){
//                            
//                            SearchView(searchText: $searchText)
//                            
//                            OnlineUsersView(users: users)
//                           
//                            Divider()
//                                .padding(.bottom, 20)
//                            
//                            VStack(spacing: 25){
//                            ForEach(messages, id: \.self) { message in
//                                NavigationLink {
//                                    ChatView().navigationBarHidden(true)
//                                    } label: {
//                                        ChatItem(
//                                            userImage: String(describing: message["user"]!),
//                                            userName: String(describing: message["user"]!),
//                                            message: String(describing: message["message"]!),
//                                            time: String(describing: message["time"]!)
//                                        )
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                .padding(.top)
//                .padding(.horizontal)
//            }
//            .navigationBarTitle("")
//            .navigationBarHidden(true)
//            .offset(y: kGuardian.slide).animation(.easeInOut(duration: 1.0))
//        }
//    }
//}
//
//struct ChatListView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChatListView().preferredColorScheme(.dark)
//    }
//}
//
