//
//  LoginView.swift
//  AppChat
//
//  Created by Tran Thuan on 23/02/2022.
//

import SwiftUI

struct Home: View {
    
    @State var status = UserDefaults.standard.value(forKey: "status") as? Bool ?? false
    
    var body: some View{
        NavigationView{
            VStack{
                if self.status{
                    HomeScreen()
                    
                } else {
                    VStack{
                        LoginView()
                    }
                     .onAppear{
                        NotificationCenter.default.addObserver(forName: NSNotification.Name("status"), object: nil, queue: .main) { (_) in
                            
                            self.status = UserDefaults.standard.value(forKey: "status") as? Bool ?? false
                        }
                    }
                }
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct HomeScreen: View{
    var body: some View{
        NavigationView {
            VStack{
                
                Image("onboard").resizable().frame(width: 300.0, height: 225.0, alignment: .center)
                
                Text("Login in successfully")
                    .font(.title)
                    .fontWeight(.bold)
                
                Button(action: {
                    
                    try! FirebaseManager.shared.auth.signOut()
                    UserDefaults.standard.set(false, forKey: "status")
                    NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
                    
                }) {
                    
                    Text("Sign out")
                        .font(.title3)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color("PrimaryColor"))
                        .cornerRadius(50)
                        .padding(.vertical)
                }
                
                NavigationLink(destination: MainMessagesView().navigationBarHidden(true)) {
                    Text("Chat list")
                        .font(.title3)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color("PrimaryColor"))
                        .cornerRadius(50)
                        .padding(.vertical)
                }
            }.padding()
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}

struct LoginView: View {
        
    @State private var textEmail: String = ""
    @State private var errorEmail: String = ""
    @State private var password: String = ""
    @State private var errorPassword: String = ""
    @State private var checkValid: Bool = false
    @State private var error = ""
    @State private var title = ""
    @State private var alert = false
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("BgColor").edgesIgnoringSafeArea(.all)
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        Image(uiImage: #imageLiteral(resourceName: "onboard"))
                        
                        Text("Login")
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .padding(.bottom, 30)
                        
                        AppChatTextField(
                            placeholder: "Email",
                            text: self.$textEmail,
                            error: self.$errorEmail
                        ).padding(.vertical)
                        
                        AppChatTextField(
                            placeholder: "Password",
                            text: self.$password,
                            error: self.$errorPassword,
                            secure: true
                        ).padding(.vertical)
                        
                        Spacer()
                        Spacer()
                        
    //                        NavigationLink(destination: ChatListView().navigationBarHidden(true), isActive: $checkValid) {
    //                            Text("Login")
    //                                .font(.title3)
    //                                .fontWeight(.bold)
    //                                .foregroundColor(.white)
    //                                .frame(maxWidth: .infinity)
    //                                .padding()
    //                                .background(Color("PrimaryColor"))
    //                                .cornerRadius(50)
    //                                .padding(.vertical)
    //                                .onTapGesture {
    //                                    checkValidationLoin()
    //                                }
    //                        }
                        // Login button
                        Button(action: {
                            self.checkValidationLoin()
                        }) {
                            Text("Login")
                                .font(.title3)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .frame(maxWidth: .infinity)
                                .padding()
                                .background(Color("PrimaryColor"))
                                .cornerRadius(50)
                                .padding(.vertical)
                        }
                        .alert(isPresented: $alert){()->Alert in
                            return Alert(title: Text("\(self.title)"), message: Text("\(self.error)"), dismissButton:
                                .default(Text("OK").fontWeight(.semibold)))
                        }
                        Spacer()
                        Spacer()
                        
                        HStack {
                            Text("New around here? ")
                            NavigationLink(destination: SignInView().navigationBarHidden(true)) {
                                Text("Sign in")
                                    .foregroundColor(Color("PrimaryColor"))
                            }
                        }
                    }
                    .padding()
                }
            }
            .ignoresSafeArea(.keyboard, edges: .bottom)
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
    }
    
    func textFieldValidatorEmail(_ string: String) -> Bool {
        if string.count > 100 {
            return false
        }
        let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        //let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: string)
    }
    
    func checkValidationLoin() {
        
        self.errorEmail = ""
        self.errorPassword = ""
        self.checkValid = false
        
        var e = 0
        
        if !self.textFieldValidatorEmail(self.textEmail) {
            self.errorEmail = "Email is Not Valid"
            e+=1
        }
        
        if password.isEmpty {
            self.errorPassword = "Password should not be empty"
            e+=1
        }
        
        if e > 0 {
            self.checkValid = false
        } else {
            self.checkValid = true
            FirebaseManager.shared.auth.signIn(withEmail: self.textEmail, password: self.password) { (res, err) in
                
                if err != nil{
                    
                    self.error = err!.localizedDescription
                    self.title = "Login Error"
                    self.alert.toggle()
                    return
                }
                
                print("Login success!")
                UserDefaults.standard.set(true, forKey: "status")
                NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
