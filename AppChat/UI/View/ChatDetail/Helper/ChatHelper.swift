////
////  ChatHelper.swift
////  AppChat
////
////  Created by Tran Thuan on 25/02/2022.
////
//
//import Combine
//
//class ChatHelper : ObservableObject {
//    var didChange = PassthroughSubject<Void, Never>()
//    @Published var realTimeMessages = DataSource.messagesDetail
//    
//    func sendMessage(_ chatMessage: Message) {
//        realTimeMessages.append(chatMessage)
//        didChange.send(())
//    }
//}
