////
////  ChatView.swift
////  AppChat
////
////  Created by Tran Thuan on 25/02/2022.
////
//
//import SwiftUI
//
//struct ChatView: View {
//    @State var typingMessage: String = ""
//    @ObservedObject private var chatHelper = ChatHelper()
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
//    @ObservedObject private var kGuardian = KeyboardGuardian(textFieldCount: 1)
//    
//    var body: some View {
//        NavigationView {
//            VStack {
//                HStack {
//                    Image("back")
//                        .frame(width: 10, height: 10, alignment: SwiftUI.Alignment.center)
//                        .foregroundColor(Color("PrimaryColor"))
//                        .aspectRatio(contentMode: .fit)
//                        .onTapGesture {
//                            self.presentationMode.wrappedValue.dismiss()
//                        }.padding()
//                    Spacer()
//                    Spacer()
//                    Text(DataSource.firstUser.name)
//                    Spacer()
//                    Spacer()
//                    Spacer()
//                }
//                List {
//                    ForEach(chatHelper.realTimeMessages, id: \.self) { msg in
//                        MessageView(currentMessage: msg)
//                    }
//                }
//                HStack {
//                    TextField("Message...", text: $typingMessage)
//                        .textFieldStyle(RoundedBorderTextFieldStyle())
//                        .frame(minHeight: CGFloat(30))
//                    Button(action: sendMessage) {
//                        Text("Send")
//                    }
//                }.frame(minHeight: CGFloat(50)).padding()
//            }
//            .navigationBarTitle("")
//            .navigationBarHidden(true)
//            .offset(y: kGuardian.slide).animation(.easeInOut(duration: 1.0))
//        }.onTapGesture {
////                self.endEditing(true)
//        }
//    }
//    
//    func sendMessage() {
//        chatHelper.sendMessage(Message(content: typingMessage, user: DataSource.secondUser))
//        typingMessage = ""
//    }
//}
//
//struct ChatView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChatView()
//    }
//}
//
