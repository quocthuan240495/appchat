//
//  SignInView.swift
//  AppChat
//
//  Created by Tran Thuan on 23/02/2022.
//

import SwiftUI
import FirebaseAuth

struct SignInView: View {
    
    @State private var offsetValue: CGFloat = 0.0
    @State private var textEmail: String = ""
    @State private var errorEmail: String = ""
    @State private var isEmailValid : Bool = true
    @State private var fullName: String = ""
    @State private var errorFullName: String = ""
    @State private var age: String = ""
    @State private var errorAge: String = ""
    @State private var address: String = ""
    @State private var errorAddress: String = ""
    @State private var password: String = ""
    @State private var errorPassword: String = ""
    @State private var rePassword: String = ""
    @State private var errorRePassword: String = ""
    @State private var alert = false
    @State private var error = ""
    @State private var alertSuccess = false
    @State private var shouldShowImagePicker = false
    @State private var image: UIImage?
    @State private var errorImage = true
    @State private var loginStatusMessage = ""
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject private var kGuardian = KeyboardGuardian(textFieldCount: 1)
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("BgColor").edgesIgnoringSafeArea(.all)
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        Group {
                            VStack {
                                VStack {
                                    Text("Sign In")
                                        .font(.largeTitle)
                                        .fontWeight(.bold)
                                        .padding(.bottom, 10)
                                    
                                    SocalLoginButton(image: Image(uiImage: #imageLiteral(resourceName: "apple")), text: Text("Sign in with Apple"))

                                    SocalLoginButton(image: Image(uiImage: #imageLiteral(resourceName: "google")), text: Text("Sign in with Google").foregroundColor(Color("PrimaryColor")))
                                        .padding(.vertical)
                                    
                                    Button {
                                        shouldShowImagePicker.toggle()
                                    } label: {
                                        VStack {
                                            if let image = self.image {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .scaledToFill()
                                                    .frame(width: 100, height: 100)
                                                    .cornerRadius(64)
                                                    .border(Color.clear, width: 0)
                                            } else {
                                                Image(systemName: "person.fill")
                                                    .font(.system(size: 64))
                                                    .padding()
                                                    .foregroundColor(Color(.label))
                                            }
                                        }
                                        .overlay(RoundedRectangle(cornerRadius: 64)
                                                    .stroke(Color.black, lineWidth: 3)
                                        )
                                    }
                                    if errorImage == false{
                                        Text("Image should not be empty")
                                            .font(.system(size: 12))
                                            .foregroundColor(.red)
                                    }
                                }
                                
                                AppChatTextField(
                                    placeholder: "Email",
                                    text: self.$textEmail,
                                    error: self.$errorEmail
                                )
                                
                                AppChatTextField(
                                    placeholder: "Full name",
                                    text: self.$fullName,
                                    error: self.$errorFullName
                                )
                                
                                AppChatTextField(
                                    placeholder: "Age",
                                    text: self.$age,
                                    error: self.$errorAge,
                                    typeNumber: true
                                )
                                
                                AppChatTextField(
                                    placeholder: "Address",
                                    text: self.$address,
                                    error: self.$errorAddress
                                )
                                
                                AppChatTextField(
                                    placeholder: "Password",
                                    text: self.$password,
                                    error: self.$errorPassword,
                                    secure: true
                                )
                                
                                AppChatTextField(
                                    placeholder: "RePassword",
                                    text: self.$rePassword,
                                    error: self.$errorRePassword,
                                    secure: true
                                )
                                
                                HStack {
                                    VStack {
                                        Button {
                                            self.checkValidationLoin()
                                        } label: {
                                            Text("Sign up")
                                                .font(.title3)
                                                .fontWeight(.bold)
                                                .foregroundColor(.white)
                                                .frame(maxWidth: .infinity)
                                                .padding()
                                                .background(Color("PrimaryColor"))
                                                .cornerRadius(50)
                                        }
                                        .alert(isPresented: self.$alert){()->Alert in
                                            return Alert(title: Text("Sign in"), message: Text("\(self.error)"), dismissButton:
                                                .default(Text("OK").fontWeight(.semibold)))
                                        }
                                        
                                        HStack {
                                            Text("Back to ")
                                            Text("Login")
                                                .foregroundColor(Color("PrimaryColor"))
                                                .onTapGesture {
                                                    self.presentationMode.wrappedValue.dismiss()
                                                }
                                        }
                                    }
                                }
                                .alert(isPresented:$alertSuccess) {
                                    Alert(
                                        title: Text("Sign in"),
                                        message: Text("You have successfully created an account"),
                                        dismissButton: .default(Text("Okay"), action: {
                                            self.presentationMode.wrappedValue.dismiss()
                                            })
                                    )
                                }
                                .padding()
                            }
                        }
                    }
                    .padding()
                }
                    
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .offset(y: kGuardian.slide).animation(.easeInOut(duration: 1.0))
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .fullScreenCover(isPresented: $shouldShowImagePicker, onDismiss: nil) {
            ImagePicker(image: $image)
        }
    }
    
    func textFieldValidatorEmail(_ string: String) -> Bool {
        if string.count > 100 {
            return false
        }
        let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        //let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: string)
    }
    
    func checkValidationLoin() {
        
        self.errorEmail = ""
        self.errorFullName = ""
        self.errorAge = ""
        self.errorAddress = ""
        self.errorPassword = ""
        self.errorRePassword = ""
        
        var e = 0
        
        if !self.textFieldValidatorEmail(self.textEmail) {
            self.errorEmail = "Email is Not Valid"
            e+=1
        }
        
        if fullName.isEmpty {
            self.errorFullName = "Full name should not be empty"
            e+=1
        }
        
        if age.isEmpty {
            self.errorAge = "Age should not be empty"
            e+=1
        }
        
        if address.isEmpty {
            self.errorAddress = "Address should not be empty"
            e+=1
        }
        
        if password.isEmpty {
            self.errorPassword = "Password should not be empty"
            e+=1
        }
        
        if rePassword.isEmpty {
            self.errorRePassword = "Password should not be empty"
            e+=1
        }
        
        if rePassword != password {
            self.errorRePassword = "RePassword does not match password"
            e+=1
        }
        let _ = print("image: \(image)")
        if image == nil{
            self.errorImage = false
            e+=1
        } else {
            self.errorImage = true
        }
        
        if e > 0 {
            
        } else {
            self.createNewAccount()
        }
    }
    
    func createNewAccount() {
        FirebaseManager.shared.auth.createUser(withEmail: textEmail, password: password) { result, err in
            if let err = err {
                print("Failed to create user:", err)
                self.loginStatusMessage = "Failed to create user: \(err)"
                return
            }
            
            print("Successfully created user: \(result?.user.uid ?? "")")
            
            self.loginStatusMessage = "Successfully created user: \(result?.user.uid ?? "")"
            
            self.persistImageToStorage()
        }
    }
    
    func persistImageToStorage() {
        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
        let ref = FirebaseManager.shared.storage.reference(withPath: uid)
        guard let imageData = self.image?.jpegData(compressionQuality: 0.5) else { return }
        ref.putData(imageData, metadata: nil) { metadata, err in
            if let err = err {
                self.loginStatusMessage = "Failed to push image to Storage: \(err)"
                return
            }
            
            ref.downloadURL { url, err in
                if let err = err {
                    self.loginStatusMessage = "Failed to retrieve downloadURL: \(err)"
                    return
                }
                
                self.loginStatusMessage = "Successfully stored image with url: \(url?.absoluteString ?? "")"
                print(url?.absoluteString)
                
                guard let url = url else {
                    return
                    
                }
                self.storeUserInformation(imageProfileUrl: url)
            }
        }
    }
    
    func storeUserInformation(imageProfileUrl: URL) {
        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
        let userData = ["email": self.textEmail, "name": self.fullName, "uid": uid, "profileImageUrl": imageProfileUrl.absoluteString]
        FirebaseManager.shared.firestore.collection("users")
            .document(uid).setData(userData) { err in
                if let err = err {
                    print(err)
                    self.loginStatusMessage = "\(err)"
                    return
                }
                
                print("Success")
                self.alertSuccess = true
            }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}

struct SocalLoginButton: View {
    var image: Image
    var text: Text
    
    var body: some View {
        HStack {
            image
                .padding(.horizontal)
            Spacer()
            text
                .font(.title2)
            Spacer()
        }
        .padding()
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .cornerRadius(50.0)
        .shadow(color: Color.black.opacity(0.08), radius: 60, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 16)
    }
}


