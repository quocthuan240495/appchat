//
//  AppChatTextField.swift
//  AppChat
//
//  Created by Tran Thuan on 24/02/2022.
//

import SwiftUI

struct AppChatTextField: View {
    
//    var icon: String
    var placeholder: String
    @Binding var text: String
    @Binding var error: String
    var secure: Bool = false
    var typeNumber: Bool = false
    @State var hide: Bool = true
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            HStack {
                
//                if !icon.isEmpty {
//                    Image(systemName: icon)
//                        .foregroundColor(Color("PrimaryColor"))
//                        .font(.system(size: 20))
//                }
                
                ZStack(alignment: .leading) {
                    
//                    if text.isEmpty || text == "" {
                        Text(placeholder)
                            .font(.system(size: 17))
                            .foregroundColor(.gray)
                            .opacity(text.isEmpty ? 1 : 0)
//                    }
                    
                    HStack {
                        if secure && hide {
                            SecureField("", text: self.$text)
                                .font(.system(size: 17))
                                .foregroundColor(.black)
                                .autocapitalization(.none)
                        }else{
                            if typeNumber {
                                TextField("", text: self.$text)
                                    .font(.system(size: 17))
                                    .foregroundColor(.black)
                                    .autocapitalization(.none)
                                    .keyboardType(UIKeyboardType.numberPad)
                            } else {
                                TextField("", text: self.$text)
                                    .font(.system(size: 17))
                                    .foregroundColor(.black)
                                    .autocapitalization(.none)
                            }
                            
                        }
                        Spacer()
                        if !text.isEmpty && !secure {
                            Image(systemName: "multiply.circle.fill")
                                .foregroundColor(.secondary)
                                .opacity(text == "" ? 0 : 1)
                                .onTapGesture { self.text = "" }
                        }
                        
                        if secure {
                            Button(action: {
                                self.hide.toggle()
                            }) {
                                Image(systemName: hide ? "eye.fill" : "eye.slash.fill")
                                    .foregroundColor(Color("PrimaryColor"))
                                    .font(.system(size: 20))
                            }
                        }
                    }
                    
                    
                }
                
            }
            .padding()
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 1).foregroundColor(self.error.isEmpty ? Color("PrimaryColor") : .red))
            
            if !error.isEmpty {
                HStack {
                    Text(error)
                        .font(.system(size: 12))
                        .foregroundColor(.red)
                }
                .padding(.horizontal, 16)
            }
            
        }
    }
}

struct AppChatTextField_Previews: PreviewProvider {
    static var previews: some View {
        AppChatTextField(
//            icon: "lock",
            placeholder: "Placeholder",
            text: .constant(""),
            error: .constant("")
        )
            .previewLayout(.fixed(width: 350, height: 100))
        .padding()
    }
}
