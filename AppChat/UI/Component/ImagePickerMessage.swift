//
//  ImagePickerMessage.swift
//  AppChat
//
//  Created by Tran Thuan on 03/03/2022.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct ImagePickerMessage: UIViewControllerRepresentable {
    
    @Binding var imageMessage: UIImage?
    private let controller = UIImagePickerController()
    
    func makeCoordinator() -> Coordinator {
        
        return Coordinator(parent: self)
        
    }
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
        let parent: ImagePickerMessage
        
        init(parent: ImagePickerMessage) {
            
            self.parent = parent
            
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            parent.imageMessage = info[.originalImage] as? UIImage
            picker.dismiss(animated: true)
            
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            
            picker.dismiss(animated: true)
            
        }
        
    }
    
    func makeUIViewController(context: Context) -> some UIViewController {
        
        controller.delegate = context.coordinator
        return controller
        
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
}
